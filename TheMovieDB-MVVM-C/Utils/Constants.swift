//
//  Constants.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import Foundation

struct Constants {
    static let host: String = "https://api.themoviedb.org/3/tv"
    static let imageBaseURL: String = "https://image.tmdb.org/t/p/w500/"
    static let apiKey: String = "4e28d55297bdf1431d184c5c5096c538"
    static let movieCell: String = "movieCell"
}
