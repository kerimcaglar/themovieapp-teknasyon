//
//  ApiKeys.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import Foundation

protocol RouteProtocol {
    var route: String { get }
}

struct ApiKeys {
    
    static let host = Constants.host
    
    enum MovieAction {
        case list
        case detail
                
        var route: String {
            switch self {
            case .list:
                return "/popular"
            case .detail:
                return "/"
            }
        }
    }
}
