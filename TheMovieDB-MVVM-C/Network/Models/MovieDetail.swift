//
//  MovieDetail.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import Foundation

struct MovieDetail: Codable {
    let backdropPath: String
    let id: Int
    let lastAirDate, name, originalLanguage: String
    let overview: String

    enum CodingKeys: String, CodingKey {
        case backdropPath = "backdrop_path"
        case id
        case lastAirDate = "last_air_date"
        case name
        case originalLanguage = "original_language"
        case overview
    }
}
