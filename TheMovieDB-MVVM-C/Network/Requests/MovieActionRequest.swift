//
//  MovieActionRequest.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import NetworkModule

enum MovieActionRequests {
    
    static func list(completion: @escaping (Result<MovieResponse, NetworkError>) -> Void ) {
        
        let request = MovieListRequest()
        let operation = NetworkOperation<MovieResponse, MovieListRequestModel>(request:request, requestModel: nil) { result in
            switch result {
            case .success(let response):
                completion(.success(response))
            case .failure(let error):
                if let error = error as? NetworkError {
                    completion(.failure(error))
                } else {
                    completion(.failure(.error(error)))
                }
            }
        }
        
        NetworkService.shared.add(operation)
    }
    
    static func detailMovie(with movieId:Int, completion: @escaping (Result<MovieDetail, NetworkError>) -> Void) {
        let request = MovieDetailRequest(movieId: movieId)
        let operation = NetworkOperation<MovieDetail, MovieDetailRequestModel>(request: request, requestModel: nil) { result in
            
            switch result {
            case .success(let response):
                completion(.success(response))
            case .failure(let error):
                if let error = error as? NetworkError {
                    completion(.failure(error))
                } else {
                    completion(.failure(.error(error)))
                }
            }
        }
        
        NetworkService.shared.add(operation)
    }
}

