//
//  MovieListRequest.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import NetworkModule
import Alamofire

class MovieListRequest: BaseRequest {
    
    override var host: String {
        return ApiKeys.host
    }
    
    override var route: String {
        return ApiKeys.MovieAction.list.route + "?api_key=" + Constants.apiKey
    }
    
    override var httpMethod: HTTPMethod {
        return .get
    }
}

struct MovieListRequestModel: Codable {}
