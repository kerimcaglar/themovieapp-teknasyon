//
//  MovieDetailRequest.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import NetworkModule
import Alamofire

class MovieDetailRequest: BaseRequest {
    
    let movieId: Int
    
    init(movieId: Int) {
        self.movieId = movieId
    }
    
    override var host: String {
        return ApiKeys.host
    }
    
    override var route: String {
        return ApiKeys.MovieAction.detail.route + "\(movieId)" +  "?api_key=" + Constants.apiKey
    }
    
    override var httpMethod: HTTPMethod {
        return .get
    }
}

struct MovieDetailRequestModel: Codable {}

