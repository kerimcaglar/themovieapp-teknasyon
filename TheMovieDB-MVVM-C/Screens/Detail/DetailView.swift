//
//  DetailView.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import UIKit
import Kingfisher

class DetailView: UIView {

    lazy var moviePosterImg: UIImageView = {
       let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFill
       return imgView
    }()
    
    lazy var movieTitle: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.textColor = .black
        lbl.font = .boldSystemFont(ofSize: 17)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var movieLanguage: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.textColor = .black
        lbl.font = .boldSystemFont(ofSize: 17)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var movieDate: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.textColor = .black
        lbl.font = .boldSystemFont(ofSize: 17)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var movieDetail: UITextView = {
        let tv = UITextView()
        tv.contentInsetAdjustmentBehavior = .automatic
        tv.isSelectable = false
        tv.isEditable = false
        tv.font = .systemFont(ofSize: 15)
        tv.textColor = .darkGray
        return tv
    }()
    
    let viewModel = DetailViewModel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setConstraints() {

        moviePosterImg.translatesAutoresizingMaskIntoConstraints = false
        movieTitle.translatesAutoresizingMaskIntoConstraints = false
        movieLanguage.translatesAutoresizingMaskIntoConstraints = false
        movieDate.translatesAutoresizingMaskIntoConstraints = false
        movieDetail.translatesAutoresizingMaskIntoConstraints = false
        addSubview(moviePosterImg)
        addSubview(movieTitle)
        addSubview(movieLanguage)
        addSubview(movieDate)
        addSubview(movieDetail)
        NSLayoutConstraint.activate([
            moviePosterImg.leadingAnchor.constraint(equalTo: leadingAnchor),
            moviePosterImg.trailingAnchor.constraint(equalTo: trailingAnchor),
            moviePosterImg.topAnchor.constraint(equalTo: topAnchor),
            moviePosterImg.heightAnchor.constraint(equalToConstant: 250),
            
            movieTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            movieTitle.topAnchor.constraint(equalTo: moviePosterImg.bottomAnchor, constant: 8),
            
            movieLanguage.leadingAnchor.constraint(equalTo: movieTitle.trailingAnchor, constant: 16),
            movieLanguage.topAnchor.constraint(equalTo: moviePosterImg.bottomAnchor, constant: 8),
            
            movieDate.leadingAnchor.constraint(equalTo: movieLanguage.trailingAnchor, constant: 16),
            movieDate.topAnchor.constraint(equalTo: moviePosterImg.bottomAnchor, constant: 8),
            
            movieDetail.leadingAnchor.constraint(equalTo: leadingAnchor),
            movieDetail.topAnchor.constraint(equalTo: movieTitle.topAnchor, constant: 20),
            movieDetail.trailingAnchor.constraint(equalTo: trailingAnchor),
            movieDetail.bottomAnchor.constraint(equalTo: bottomAnchor)

        ])
    }
    
    func configure(for movieDetail: MovieDetail) {
        self.moviePosterImg.kf.setImage(with: self.viewModel.getImageURLString(for: movieDetail))
        self.movieTitle.text = movieDetail.name
        self.movieLanguage.text = movieDetail.originalLanguage
        self.movieDate.text = movieDetail.lastAirDate
        self.movieDetail.text = movieDetail.overview
    }
}
