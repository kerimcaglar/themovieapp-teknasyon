//
//  DetailViewModel.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import UIKit
import RxSwift
import RxCocoa

struct DetailViewModel {
    
    var item = PublishSubject<MovieDetail>()
    var moviePosterImage: BoxBind<UIImage?> = BoxBind(nil)
    
    func fetchMovie(movieId: Int) {
        MovieActionRequests.detailMovie(with: movieId) { (result) in
            switch result {
            case .success(let movie):
                item.onNext(movie)
                item.onCompleted()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func getImageURLString(for movie: MovieDetail) -> URL? {
        let posterPathStr = Constants.imageBaseURL + movie.backdropPath
        guard let imageURL = URL(string: posterPathStr) else { return nil }
        return imageURL
    }
}
