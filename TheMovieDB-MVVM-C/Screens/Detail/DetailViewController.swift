//
//  DetailViewController.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import UIKit
import RxSwift

class DetailViewController: UIViewController {

    weak var coordinator: MainCoordinator?
    var movieId: Int?
    private let viewModel = DetailViewModel()
    let disposeBag = DisposeBag()
    private let detailView: DetailView = DetailView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view = detailView
        getMovieDetail()
    }
    
    private func getMovieDetail() {
        guard let movieId = movieId else { return }
        viewModel.item.subscribe { [weak self] (movieDetail) in
            guard let self = self else { return }
            self.title = movieDetail.name
            self.detailView.configure(for: movieDetail)
        } onError: { (error) in
            print(error.localizedDescription)
        } onCompleted: {
            print("Completed")
        } onDisposed: {
            print("Disposed")
        }.disposed(by: disposeBag)

        viewModel.fetchMovie(movieId: movieId )

    }

}
