//
//  ListViewModel.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import UIKit
import RxCocoa
import RxSwift

struct ListViewModel {
    
    var items = PublishSubject<[Movie]>()
    var moviePosterImage: BoxBind<UIImage?> = BoxBind(nil)
    
    func fetchMovies() {
        MovieActionRequests.list { (result) in
            switch result {
            case .success(let movies):
                items.onNext(movies.results)
                items.onCompleted()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func getImageURLString(for movie: Movie) -> URL? {
        let posterPathStr = Constants.imageBaseURL + movie.posterPath
        guard let imageURL = URL(string: posterPathStr) else { return nil }
        return imageURL
    }
}
