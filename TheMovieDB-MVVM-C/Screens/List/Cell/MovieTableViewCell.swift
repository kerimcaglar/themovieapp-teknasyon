//
//  MovieTableViewCell.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import UIKit
import Kingfisher

class MovieTableViewCell: UITableViewCell, ComponentShimmers {

    lazy var moviePosterImg: UIImageView = {
       let imgView = UIImageView()
       return imgView
    }()
    
    lazy var movieTitle: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.textColor = .red
        lbl.font = .boldSystemFont(ofSize: 16)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var movieRating: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textColor = .black
        lbl.numberOfLines = 0
        lbl.sizeToFit()
        return lbl
    }()
    
    let animationDuration: Double = 0.25
    var shimmer: ShimmerLayer = ShimmerLayer()
    let viewModel = ListViewModel()
    
    func hideViews() {
        ViewAnimationFactory.makeEaseOutAnimation(duration: animationDuration, delay: 0) {
            self.moviePosterImg.setOpacity(to: 0)
            self.movieTitle.setOpacity(to: 0)
            self.movieRating.setOpacity(to: 0)
        }
    }
    
    func showViews() {
        ViewAnimationFactory.makeEaseOutAnimation(duration: animationDuration, delay: 0) {
            self.moviePosterImg.setOpacity(to: 1)
            self.movieTitle.setOpacity(to: 1)
            self.movieRating.setOpacity(to: 1)
        }
    }
    
    func setShimmer() {
        DispatchQueue.main.async { [unowned self] in
            self.shimmer.removeLayerIfExists(self)
            self.shimmer = ShimmerLayer(for: self.moviePosterImg, cornerRadius: 12)
            self.layer.addSublayer(self.shimmer)
        }
    }
    
    func removeShimmer() {
        shimmer.removeFromSuperlayer()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
       
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setConstraints() {
                
        let innerStackView = UIStackView(arrangedSubviews: [movieTitle, movieRating])
        innerStackView.axis = .vertical
        
        let outerStackView = UIStackView(arrangedSubviews: [moviePosterImg, innerStackView])
        outerStackView.translatesAutoresizingMaskIntoConstraints = false
        outerStackView.alignment = .center
        outerStackView.spacing = 10
        contentView.addSubview(outerStackView)
        
        NSLayoutConstraint.activate([
            moviePosterImg.widthAnchor.constraint(equalToConstant: 100),
            moviePosterImg.heightAnchor.constraint(equalToConstant: 100),
            outerStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            outerStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            outerStackView.topAnchor.constraint(equalTo: contentView.topAnchor)

        ])
        
    }
    
    func configure(movie: Movie) {
        setShimmer()
        let imageURL = viewModel.getImageURLString(for: movie)
        self.moviePosterImg.kf.setImage(with: imageURL)
        self.movieTitle.text = movie.name
        self.movieRating.text = "\(movie.voteAverage)"
        
        DispatchQueue.global().async {
            self.viewModel.moviePosterImage.bind {
                guard let posterImage = $0 else { return }
                DispatchQueue.main.async { [unowned self] in
                    self.moviePosterImg.image = posterImage
                    self.removeShimmer()
                    self.showViews()
                }
            }
        }
    }
}

