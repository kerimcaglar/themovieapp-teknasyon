//
//  ListViewController.swift
//  TheMovieDB-MVVM-C
//
//  Created by Kerim Caglar on 27.10.2021.
//

import UIKit
import RxSwift
import Kingfisher

class ListViewController: UIViewController, UIScrollViewDelegate {
    
    weak var coordinator: MainCoordinator?
    
    private let tableView: UITableView = {
        let table = UITableView()
        table.register(MovieTableViewCell.self, forCellReuseIdentifier: Constants.movieCell)
        return table
    }()
    
    private var viewModel = ListViewModel()
    private var disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        title = "Popular TV"
        tableView.frame = view.bounds
        tableView.rx.setDelegate(self).disposed(by:disposeBag)
        bindData()
    }
    
    private func bindData() {
        //Bind data with tableview
        viewModel.items.bind(to: tableView.rx.items(
                                cellIdentifier: Constants.movieCell,
                                cellType: MovieTableViewCell.self))
        { (row, movie, cell) in
            cell.configure(movie: movie)
        }.disposed(by: disposeBag)
        
        //Selected a row, show detail
        tableView.rx.modelSelected(Movie.self).bind { movie in
            self.coordinator?.detail(with: movie.id)
            print(movie.name)
        }.disposed(by: disposeBag)
        
        viewModel.fetchMovies()
    }
}

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}

