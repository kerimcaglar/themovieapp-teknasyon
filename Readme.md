## Project Tech Stack

* Xcode: Version 12.4 (12D4e)
* Language: Swift 5.3.2
* Minimum iOS Version: 11.0
* Design Pattern: MVVM - C
* Dependency  Manager: -
* Dependencies: -
* Style Guide: [Raywenderlich](https://github.com/raywenderlich/swift-style-guide)
* Made with ❤️

## Some Highlighted Things

- ✅ RxSwift (Design & Network)
- ✅ Programmatically design
- ✅ Organized folder structure
- ✅ Custom views
- ✅ Readability
- ✅ Following same styling in every file

## MVVM-C Architecture
![enter image description here](https://i.ibb.co/8KZ5dHv/mvvm-c.png)

## ToDo - Some Issues
-  Unit tests for MVVM-C with RxTest
